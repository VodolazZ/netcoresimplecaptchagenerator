﻿namespace SimpleCaptchaGenerator
{
    using System.Drawing.Imaging;
    using System.Text;
    using System;
    using System.Collections.Generic;
    using System.Drawing;

    public class CaptchaGenerator
    {
        public static SettingsContainer GetSettingsContainer => new SettingsContainer();
        private static Random randomizer;

        private readonly SettingsContainer captchaSettings;

        private readonly int maxFontTypeCount;
        private readonly int availibleSymbols;
        private const int MIN_INDENT = 3;
        private const float PT_TO_PIX_FACTOR = 96f / 72;

        private int maxFontSizeValue;
        private int minFontSizeValue;

        private int availibleOffsetCount;
        private readonly int maxHorizontalOffset;

        public CaptchaGenerator(SettingsContainer captchaSettings)
        {
            randomizer = new Random();
            this.captchaSettings = captchaSettings;
            this.maxFontTypeCount = Enum.GetValues(typeof(Fonts)).Length;

            this.maxFontSizeValue = this.CalculateMaxFontSize();

            if (this.maxFontSizeValue < 10)
            {
                this.minFontSizeValue = this.maxFontSizeValue;
            }
            else
            {
                this.minFontSizeValue = this.maxFontSizeValue - 5;
            }

            this.availibleOffsetCount = (int)(captchaSettings.PictureWidth -
                                        captchaSettings.CodeLength * this.maxFontSizeValue * PT_TO_PIX_FACTOR);

            this.maxHorizontalOffset = this.availibleOffsetCount / 3;

            switch (captchaSettings.CaptchaContType & CaptchaContentType.LatinLettersAndNumbers)
            {
                case CaptchaContentType.LatinLetters:
                    availibleSymbols = 25;
                    break;
                case CaptchaContentType.LatinLettersAndNumbers:
                    availibleSymbols = 35;
                    break;
                case CaptchaContentType.Numbers:
                    availibleSymbols = 10;
                    break;
                default:
                    throw new ArgumentException("wrong CaptchaContentType detected in the constructor");
            }
        }

        /// <summary>
        /// Generates captcha, writes it to provided filename and returns its string representation
        /// </summary>
        /// <param name="fileToSavePicture">file name for saving captcha</param>
        /// <param name="fileFormat">captcha image file format</param>
        /// <returns>captcha string representation</returns>
        public string GenerateCaptcha(string fileToSavePicture, ImageFormat fileFormat = null)
        {
            if (fileFormat == null)
            {
                fileFormat = ImageFormat.Gif;
            }

            StringBuilder captchaKey = new StringBuilder();
            using (Bitmap captBitmap = new Bitmap(this.captchaSettings.PictureWidth, this.captchaSettings.PictureHeight))
            {
                using (Graphics captGraph = Graphics.FromImage(captBitmap))
                {
                    captGraph.Clear(this.captchaSettings.BackGroundColor);

                    for (int symbol = 0; symbol < this.captchaSettings.CodeLength; symbol++)
                    {
                        Font nextSymbolFont = this.GenerateRandomFont();
                        string nextSymbol = this.GenerateRandomChar();

                        captchaKey.Append(nextSymbol);
                        captGraph.DrawString(nextSymbol, nextSymbolFont, this.captchaSettings.CodeColor, 
                            this.maxFontSizeValue * PT_TO_PIX_FACTOR * symbol + this.GetRandomHorizontalOffset(), 
                            this.GetRandomVerticalOffset(nextSymbolFont));
                        nextSymbolFont.Dispose();
                    }

                    captBitmap.Save(fileToSavePicture, fileFormat);
                }
            }
            return captchaKey.ToString();
        }

        private Font GenerateRandomFont()
        {
            int fontType = randomizer.Next(0, this.maxFontTypeCount);
            int fontSize = randomizer.Next(this.minFontSizeValue, this.maxFontSizeValue);
            int fontStyle = randomizer.Next(0, 16);
            return new Font(this.fontsNamesCollection[(Fonts) fontType], fontSize, (FontStyle) fontStyle);
        }

        private int CalculateMaxFontSize()
        {
            int maxSizeFromWidth = (int)((float)(this.captchaSettings.PictureWidth - MIN_INDENT * (this.captchaSettings.CodeLength - 1)) /
                this.captchaSettings.CodeLength
                / PT_TO_PIX_FACTOR);
            int maxSizeFromHeight = (int)(this.captchaSettings.PictureHeight / PT_TO_PIX_FACTOR / 2);

            return Math.Min(maxSizeFromHeight, maxSizeFromWidth);
        }

        private string GenerateRandomChar()
        {
            int randomIndex = randomizer.Next(0, availibleSymbols);
            int unicodeSymbol = GetUnicodeSymbolFromRandomIndex(randomIndex);
            string unicodeChar = char.ConvertFromUtf32(unicodeSymbol);
            return unicodeChar;
        }

        private int GetUnicodeSymbolFromRandomIndex(int index)
        {
            switch (captchaSettings.CaptchaContType & CaptchaContentType.LatinLettersAndNumbers)
            {
                case CaptchaContentType.LatinLetters:
                    return index + 65;
                case CaptchaContentType.Numbers:
                    return index + 48;
                case CaptchaContentType.LatinLettersAndNumbers:
                    return index < 10 ? index + 48 : index + 55;
                default:
                    throw new ArgumentException("received wrong index in GetUnicodeSymbolFromRandomIndex");
            }
        }

        private int GetRandomVerticalOffset(Font currentFont)
        {
            int maxAvailibleOffset = this.captchaSettings.PictureHeight - currentFont.Height;
            return randomizer.Next(0, maxAvailibleOffset);
        }

        private int GetRandomHorizontalOffset()
        {
            if (this.availibleOffsetCount <= 0)
            {
                return MIN_INDENT;
            }

            int offset = randomizer.Next(MIN_INDENT, this.maxHorizontalOffset);
            this.availibleOffsetCount -= offset;
            return offset;
        }

        private Dictionary<Fonts, string> fontsNamesCollection = new Dictionary<Fonts, string>()
        {
            {Fonts.Arial, "Arial"},
            {Fonts.TimesNewRoman, "Times New Roman"},
            {Fonts.Verdana, "Verdana"},
            {Fonts.ComicSans, "Comic Sans" }
        };
    }

    public class SettingsContainer
    {
        
        private int codeLength;

        public int PictureWidth { get; set; }
        public int PictureHeight { get; set; }
        
        public int CodeLength
        {
            get { return this.codeLength;}
            set
            {
                if (value >= 3 && value <= 8)
                    this.codeLength = value;
                else this.codeLength = 4;
            }
        }

        public CaptchaContentType CaptchaContType { get; set; }
        public Color BackGroundColor { get; set; }
        public Brush CodeColor { get; set; }

        internal SettingsContainer()
        { }
    }

    [Flags]
    public enum CaptchaContentType : byte
    {
        LatinLetters = 1,
        Numbers = 2,
        LatinLettersAndNumbers = LatinLetters | Numbers
    }

    internal enum Fonts : byte
    {
        Arial,
        TimesNewRoman,
        Verdana,
        ComicSans
    }
}
